# {{name}}

> {{description}}

``` bash
Build Setup
# install dependencies
npm install

# NW.js with hot
npm start

# build NW.js application for production
npm run build
```

hr

This project was generated with [NW.js with Vue and JavaScript](https://gitlab.com/patrickalima98/nwjs-with-vue) using vue-cli.