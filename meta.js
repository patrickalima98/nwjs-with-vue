module.exports = {
  prompts: {
    name: {
      type: 'string',
      required: true,
      message: 'Project name',
    },
    description: {
      type: 'string',
      required: false,
      message: 'Project description',
      default: 'A NW.js project with Vue.js',
    },
    label: {
      type: 'string',
      required: false,
      message: 'Application conventional name',
      default (data) {
        return data.name.replace(/((?:^|-|_)+[a-z])/g, ($1) => $1.toUpperCase().replace('-', ''))
      }
    },
    author: {
      type: 'string',
      message: 'Author',
      required: false
    },
    plugins: {
      type: 'checkbox',
      message: 'Select which Vue plugins to install',
      choices: ['axios', 'vue-router', 'vuex'],
      default: ['axios', 'vue-router', 'vuex']
    }
  },
  helpers: {
    isEnabled (list, check, opts) {
      if (list[check]) return opts.fn(this)
      else return opts.inverse(this)
    }
  },
  filters: {
    'app/router/**/*': 'plugins[\'vue-router\']',
    'app/store/**/*': 'plugins[\'vuex\']'
  },
  complete (data) {
    console.log()
    console.log('   Welcome to your new NW.js app with Vue.js :)')
    console.log()
    console.log('   Next steps:')
    if (!data.inPlace) console.log(`      \x1b[33m$\x1b[0m cd ${data.destDirName}`)
    console.log('      \x1b[33m$\x1b[0m npm install')
    console.log('      \x1b[33m$\x1b[0m npm start')
  }
}